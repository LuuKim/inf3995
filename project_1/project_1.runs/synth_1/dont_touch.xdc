# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# Block Designs: bd/essai1/essai1.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1 || ORIG_REF_NAME==essai1} -quiet] -quiet

# IP: bd/essai1/ip/essai1_processing_system7_0_0/essai1_processing_system7_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1_processing_system7_0_0 || ORIG_REF_NAME==essai1_processing_system7_0_0} -quiet] -quiet

# IP: bd/essai1/ip/essai1_axi_gpio_0_0/essai1_axi_gpio_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1_axi_gpio_0_0 || ORIG_REF_NAME==essai1_axi_gpio_0_0} -quiet] -quiet

# IP: bd/essai1/ip/essai1_auto_pc_0/essai1_auto_pc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1_auto_pc_0 || ORIG_REF_NAME==essai1_auto_pc_0} -quiet] -quiet

# IP: bd/essai1/ip/essai1_ps7_0_axi_periph_0/essai1_ps7_0_axi_periph_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1_ps7_0_axi_periph_0 || ORIG_REF_NAME==essai1_ps7_0_axi_periph_0} -quiet] -quiet

# IP: bd/essai1/ip/essai1_rst_ps7_0_100M_0/essai1_rst_ps7_0_100M_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==essai1_rst_ps7_0_100M_0 || ORIG_REF_NAME==essai1_rst_ps7_0_100M_0} -quiet] -quiet

# XDC: bd/essai1/essai1_ooc.xdc
