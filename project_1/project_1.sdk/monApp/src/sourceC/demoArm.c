/*
*  Jérôme Collin - Test de base du ARM avec quelques périphériques simples
*
*  Juillet 2015
*
*  Le code fait ceci:
*    - à chaque 2 secondes, le timer génère une interruption qui
*      fait afficher par RS232 des caractères au terminal.
*    - Appuyer sur le bouton PB1 fait allumer la led LD9 juste à côté.
*    - Dans une boucle infinie, la valeur des 8 interrupteurs à glissière
*      fait allumer les 8 leds correspondantes au-dessus.
*
*  Références utiles:
*  http://forums.xilinx.com/t5/Xcell-Daily-Blog/Implementing-the-Zynq-SoC-s-Private-Timer-Adam-Taylor-s-MicroZed/ba-p/402203
*  http://svenand.blogdrive.com/archive/174.html#.Vbgi4_lITap
*  http://forums.xilinx.com/t5/Xcell-Daily-Blog/Driving-the-Zynq-SoC-s-GPIO-Adam-Taylor-s-MicroZed-Chronicles/ba-p/389611
*  http://www.xilinx.com/support/documentation/xcell_articles/how-to-use-interrupts-on-zynqsoc.pdf
*  http://infocenter.arm.com/help/topic/com.arm.doc.ddi0407f/DDI0407F_cortex_a9_r2p2_mpcore_trm.pdf
*/

/*#include <stdio.h>*/
#include "platform.h"
#include "xgpiops.h"
#include "xgpio.h"
#include "xscutimer.h"
#include "xscugic.h"
#include "xil_exception.h"

/* Boutons-poussoirs 1 et 2 accessibles par le MIO vers le PS
* pour usage direct par le ARM.  Les 5 autres BTNU, BTND,
* BTNC, BTNL et BTNR doivent passer par le PL pour être utilisés.
*/
#define PB1 50
#define PB2 51
#define LD9 7

/*
 * Utile pour recharger le timer SCU de 32 bits
 * Normalement, le CPU est à 666 MHz.  Le timer
 * roule à la moitié de cette valeur. La documentation
 * officielle du ARM dit que le prescaler agit
 * de la façon suivante:
 * ( ( PRESCALAR + 1 ) * (LOAD_VALUE + 1 ) ) /  PERIPHCLOCK
 * Comme je n'ai pas employe de prescalar ici, la valeur 0x28000000
 * chargée fait que le timer devrait générer une interruption
 * au deux secondes.  A l'oeil, au terminal, c'est pas mal
 * ce que j'obtiens...  J'en conclus que c'est bon!
 *
 */
#define TIMER_LOAD_VALUE  0x28000000

/* variables globales pour les GPIO */
XGpioPs GpioMIO;
XGpio   GpioAXI;
int Status;
XGpioPs_Config *GPIO_MIOConfigPtr;
XGpio_Config   *GPIO_AXIConfigPtr;

/* variables globales pour le timer */
XScuTimer_Config *TMRConfigPtr;
XScuTimer Timer;

/* variables pour le gestionnaire d'interruptions GIC */
XScuGic InterruptController;
XScuGic_Config *GicConfigPtr;

int configGPIOs () {

  /* en premier, celui accessible par le MIO pour les PB1 et PB2 */
  GPIO_MIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
  Status = XGpioPs_CfgInitialize(&GpioMIO, GPIO_MIOConfigPtr, GPIO_MIOConfigPtr->BaseAddr);

  if (Status != XST_SUCCESS)
      return XST_FAILURE;

  XGpioPs_SetDirectionPin(&GpioMIO, PB1, 0); /* 0: lecture, en entree */
  XGpioPs_SetDirectionPin(&GpioMIO, PB2, 0);
  XGpioPs_SetDirectionPin(&GpioMIO, LD9, 1); /* 1: écriture, en sortie */

  XGpioPs_SetOutputEnablePin(&GpioMIO, PB1, 0); /* 0: disable output enable */
  XGpioPs_SetOutputEnablePin(&GpioMIO, PB2, 0); /* veut dire input enable ? */
  XGpioPs_SetOutputEnablePin(&GpioMIO, LD9, 1); /* 1: enable output enable */

  /* en deuxieme, celui accessible du PL par le AXI */
  GPIO_AXIConfigPtr = XGpio_LookupConfig(XPAR_AXI_GPIO_0_DEVICE_ID);
  /* BaseAddress et non BaseAddr... Les deux API ne sont pas semblables! */
  Status = XGpio_CfgInitialize(&GpioAXI, GPIO_AXIConfigPtr, GPIO_AXIConfigPtr->BaseAddress);

  if (Status != XST_SUCCESS)
     return XST_FAILURE;

  XGpio_SetDataDirection (&GpioAXI, 1 /* channel 1 */ , 0 /* direction 0: out */ );
  XGpio_SetDataDirection (&GpioAXI, 2 /* channel 2 */ , 0 /* direction 1: in */ );

  return XST_SUCCESS;
}


void TimerIntrHandler(void *CallBackRef)
{
   /* juste pour compter de façon pas très brillante le
    * nombre de fois qu'on entre dans la routine...
    */
   static uint32_t nbr = 0;
   XScuTimer *TimerInstancePtr = (XScuTimer *) CallBackRef;
   XScuTimer_ClearInterruptStatus(TimerInstancePtr);
      xil_printf("===> Interruption du timer SCU: %d\n\r", nbr++);
}


void setTimerAndIntr() {
  /* la base avec le timer */
  TMRConfigPtr = XScuTimer_LookupConfig(XPAR_PS7_SCUTIMER_0_DEVICE_ID);
  XScuTimer_CfgInitialize(&Timer, TMRConfigPtr,TMRConfigPtr->BaseAddr);
  XScuTimer_SelfTest(&Timer);

  /* la base avec le gestionnaire d'interruptions */
  GicConfigPtr = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);
  XScuGic_CfgInitialize(&InterruptController, GicConfigPtr,
					    GicConfigPtr->CpuBaseAddress);
  XScuGic_SelfTest(&InterruptController);
  /*SetUpInterruptSystem(&InterruptController); */

  Xil_ExceptionInit();
  Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, &InterruptController);
  Xil_ExceptionEnable();

  /* connecter le timer au gestionnaire */
  XScuGic_Connect(&InterruptController, XPAR_SCUTIMER_INTR,
                  (Xil_ExceptionHandler)TimerIntrHandler,
                  (void *)&Timer);

  /* enable the interrupt for the Timer at GIC  */
  XScuGic_Enable(&InterruptController, XPAR_SCUTIMER_INTR);

  /* on veut repartir le compteur automatiquement après chaque interruption */
  XScuTimer_EnableAutoReload(&Timer);
  XScuTimer_EnableInterrupt	(&Timer);

  /* Charger le timer et le partir */
  XScuTimer_LoadTimer(&Timer, TIMER_LOAD_VALUE);
  XScuTimer_Start(&Timer);
}


int main()
{
	int value;

    init_platform();
    configGPIOs();
    setTimerAndIntr();

    print("Debut du programme\n\r");

    while (1) {
       /* PB1: allumer la led LD9 */
       value = XGpioPs_ReadPin(&GpioMIO, PB1);
       if (value == 1)
    	   XGpioPs_WritePin(&GpioMIO, LD9, 1);
       else
    	   XGpioPs_WritePin(&GpioMIO, LD9, 0);

       /* PB2: juste afficher une string, rien de compliqué */
       value = XGpioPs_ReadPin(&GpioMIO, PB2);
       if (value == 1) {
           value = XScuTimer_GetCounterValue(&Timer);
    	   xil_printf ("bouton PB2 enfoncé. Counteur: %X\n\r", value);
       }

  	   /* Afficher les DEL correspondant aux interrupteurs */
  	   value = XGpio_DiscreteRead (&GpioAXI, 2);
       XGpio_DiscreteWrite (&GpioAXI, 1, value);
    }

    print("ERREUR: Ne devrait jamais s'afficher...\n\r");

    cleanup_platform();
    return 0;
}
